﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT.Validate
{
    class ValidatePX
    {
        private bool isNumber(TextBox txt)
        {
            String str = txt.Text;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] < 48 || str[i] > 57)
                {
                    return false;
                }
            }
            return true;
        }

        private bool isEmpty(TextBox txt)
        {
            if (txt.Text == "")
            {
                return true;
            }
            return false;
        }

     
        public bool validate(TextBox txtTEN)
        {
            
            if (isEmpty(txtTEN))
            {
                MessageBox.Show("Họ tên khách hàng không được để trống!", "THÔNG BÁO GHI", MessageBoxButtons.OK);
                txtTEN.Focus();
                return false;
            }
            if (isNumber(txtTEN))
            {
                MessageBox.Show("Họ tên khách hàng phải là chữ!", "THÔNG BÁO GHI", MessageBoxButtons.OK);
                txtTEN.Focus();
                return false;
            }
         

            return true;
        }
    }
}
