﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using QLVT.Validate;
using System.Text.RegularExpressions;

namespace QLVT
{
    public partial class fmNhanVien : Form
    {
        Boolean isEdit ;
        int vitri = 0;
        private string maCN ;
        private ValidateNhanVien validateNhanVien;
        
        public fmNhanVien()
        {
            InitializeComponent();
            gridView1.OptionsBehavior.Editable = false;
          
            trangThai2();

            panel1.Visible = false;
            if (Program.mGroup == Program.nhomQuyen[2])
            {
                btnDK.Enabled = btnChuyenCN.Enabled = false;
                btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled = false;
                btnUndo.Enabled = btnRedo.Enabled = false;
            }
            else if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false;
                btnChuyenCN.Enabled = btnDK.Enabled = true;
                btnUndo.Enabled = btnRedo.Enabled = false;
                panel1.Visible = true;
            }
            else
            {
                btnDK.Enabled = true;
            }

            this.validateNhanVien = new ValidateNhanVien();
        }

        private void fmNhanVien_Load(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
            //this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
            this.nhanVienTableAdapter.Fill(this.qLVT_DATHANGDataSet1.NhanVien);

            if (bdsNV.Count > 0)
            {
                this.maCN = ((DataRowView)bdsNV[0])["MACN"].ToString();
                Program.maCH = this.maCN;
                txtMACN.Text = this.maCN;
                String maNV= ((DataRowView)bdsNV[0])["MANV"].ToString();
                String ttx = ((DataRowView)bdsNV[0])["TrangThaiXoa"].ToString();
                if (Program.mGroup != Program.nhomQuyen[2])
                {
                    this.checkAcount(maNV, ttx);
                }
            }
            else
            {
                btnDK.Enabled = false;
                btnSua.Enabled = btnXoa.Enabled = false;
            }
           
            cmbCN.SelectedIndex = Program.mChinhanh;
        }

        private void trangThai1()
        {
            gcNV.Enabled = false;
            txtDIACHI.Enabled = txtHO.Enabled = txtLUONG.Enabled = txtMACN.Enabled
                = txtTEN.Enabled = txtNGAYSINH.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled  = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled 
                = btnIn.Enabled = btnRefesh.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;
            cmbCN.Enabled = false;

        }

        private void trangThai2()
        {
            txtDIACHI.Enabled = txtHO.Enabled = txtLUONG.Enabled = txtMACN.Enabled
          = txtMANV.Enabled = txtTEN.Enabled = txtNGAYSINH.Enabled = false;

            btnDK.Enabled = false;
            btnChuyenCN.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnRefesh.Enabled = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;
            gcNV.Enabled = true;
            cmbCN.Enabled = true;
        }

        private void checkAcount(String maNV,String ttx)
        {
            if (ttx == "0")
            {
                if (Program.mGroup == Program.nhomQuyen[1])
                {
                    btnChuyenCN.Enabled = true;
                }
              
                if (SP.checkIsExistAccount(maNV))
                {
                    btnDK.Enabled = false;
                    btnDK.Caption = "Đã Đăng Kí";
                }
                else
                {
                    btnDK.Enabled = true;
                    btnDK.Caption = "Đăng Kí";
                }
            }
            else
            {
                btnDK.Enabled = false;
                if (SP.checkIsExistAccount(maNV))
                {
                    if (Program.mGroup == Program.nhomQuyen[1])
                    {
                        btnChuyenCN.Enabled = true;
                    }
                    btnDK.Caption = "Đã Đăng Kí";
                }
                else
                {
                    btnChuyenCN.Enabled = false;
                    btnDK.Caption = "Đã Chuyển CN";
                }
            }
        }

        private void updateTableAdapter()
        {
            bdsNV.EndEdit();
            bdsNV.ResetCurrentItem();
            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
            this.nhanVienTableAdapter.Update(this.qLVT_DATHANGDataSet1.NhanVien);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            vitri = bdsNV.Position;
            bdsNV.AddNew();

            isEdit = false;

            txtMANV.Text = SP.initMaNV().ToString();

            txtMACN.Text = this.maCN;
            txtTTX.Text = "0";
            txtLUONG.Text = "7,000,000.00";
            btnDK.Enabled = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            vitri = bdsNV.Position;
            isEdit = true;
            
            this.trangThai1();
            txtMANV.Enabled = false;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            if (gridView1.GetFocusedDataRow()["TrangThaiXoa"].ToString() == "0")
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (txtMANV.Text.Trim() == Program.username)
                    {
                        MessageBox.Show("Tài khoản này đang được sử dụng!", "THÔNG BÁO", MessageBoxButtons.OK);
                        return;
                    }
                    else
                    {
                        String loginName = RemoveVietnameseTone(txtTEN.Text).Replace(" ", "") + txtMANV.Text;
                        if(!SP.delLogin(loginName,txtMANV.Text))
                        {
                            return;
                        }
                        
                        bdsNV.RemoveCurrent();
                        this.updateTableAdapter();
                        MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                MessageBox.Show("Không Thể Xóa", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            trangThai2();
            this.refesh();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtMANV.Text = txtMANV.Text.Trim();
            txtHO.Text = txtHO.Text.Trim();
            txtTEN.Text = txtTEN.Text.Trim();
            txtDIACHI.Text = txtDIACHI.Text.Trim();
            txtLUONG.Text = txtLUONG.Text.Trim();

            if (!validateNhanVien.validate(txtMANV,txtHO,txtTEN,txtDIACHI,txtLUONG))
            {
                return;
            }

         

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }
            this.trangThai2();
            this.refesh();
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

       

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(!isEdit)
            {
                bdsNV.RemoveCurrent();
            }
            trangThai2();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MACN"], this.maCN);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["TrangThaiXoa"],0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["NGAYSINH"], txtNGAYSINH.Value.Date);
        }

        private void cbCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCN.SelectedValue!=null)
            {
                Program.servername = cmbCN.SelectedValue.ToString();

                if (cmbCN.SelectedIndex != Program.mChinhanh)
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                else
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }

                if (Program.KetNoi() == 0)
                {
                    MessageBox.Show("Loi");
                }
                else
                {
                   
                    this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                    //this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
                    this.nhanVienTableAdapter.Fill(this.qLVT_DATHANGDataSet1.NhanVien);
                    this.maCN = ((DataRowView)bdsNV[0])["MACN"].ToString();
                }
            }
          
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void gcNV_Click(object sender, EventArgs e)
        {
            if (Program.mGroup != Program.nhomQuyen[2])
            {
                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    string maNV = gridView1.GetRowCellValue(selectedRowHandles[0], "MANV").ToString();
                    string ttx = gridView1.GetRowCellValue(selectedRowHandles[0], "TrangThaiXoa").ToString();
                    this.checkAcount(maNV, ttx);

                }
            }
        }

        public string RemoveVietnameseTone(string text)
        {
            string result = text.ToLower();
            result = Regex.Replace(result, "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|/g", "a");
            result = Regex.Replace(result, "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|/g", "e");
            result = Regex.Replace(result, "ì|í|ị|ỉ|ĩ|/g", "i");
            result = Regex.Replace(result, "ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|/g", "o");
            result = Regex.Replace(result, "ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|/g", "u");
            result = Regex.Replace(result, "ỳ|ý|ỵ|ỷ|ỹ|/g", "y");
            result = Regex.Replace(result, "đ", "d");
            return result;
        }

        private void btnDK_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String loginName = RemoveVietnameseTone(txtTEN.Text).Replace(" ","") + txtMANV.Text;
            String userName = txtMANV.Text;

            if (Program.mGroup == Program.nhomQuyen[0])
            {
                new fmRegister(loginName, userName).ShowDialog();
            }
            else if (Program.mGroup == Program.nhomQuyen[1])
            {
                new fmRegister(loginName, userName, Program.nhomQuyen[1]).ShowDialog();
            }

            this.refesh();
        }

        private void btnChuyenCN_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (txtMANV.Text.Trim() == Program.username)
            {
                MessageBox.Show("Tài khoản này đang được sử dụng!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            String name = RemoveVietnameseTone(txtTEN.Text).Replace(" ", "");
            String loginName = name + txtMANV.Text;
            String userName = txtMANV.Text;
            bool isRegister = !btnDK.Enabled;
           
            new fmChuyenCN(loginName,userName, name,this.maCN,isRegister).ShowDialog();

            this.refesh();
        }

        private void refesh()
        {
            this.nhanVienTableAdapter.Fill(this.qLVT_DATHANGDataSet1.NhanVien);
        }

    }
}
