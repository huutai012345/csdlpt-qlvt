﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;

namespace QLVT
{
    public partial class fmDangNhap : Form
    {
        public fmDangNhap()
        {
            InitializeComponent();
        }

        public fmDangNhap(String loginName,String pass)
        {
            InitializeComponent();

            txtLogin.Text = loginName;
            txtPass.Text = pass;
        }

        private void fmDangNhap_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
        
            cmbCN.SelectedIndex = 0;
            Program.servername = cmbCN.SelectedValue.ToString();
        }

        private void tENCNComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCN.SelectedValue == null)
            {
                return;
            }                
            Program.servername = cmbCN.SelectedValue.ToString();
            
        }

        private void btnDN_Click(object sender, EventArgs e)
        {
            txtLogin.Text = txtLogin.Text.Trim();
            txtPass.Text = txtPass.Text.Trim();

            if (txtLogin.Text == "")
            {
                MessageBox.Show("Tài khoản không được rỗng", "Báo lỗi đăng nhập", MessageBoxButtons.OK);
                txtLogin.Focus();
                return;
            }
            if (txtPass.Text == "")
            {
                MessageBox.Show("Vui lòng điền mật khẩu", "Báo lỗi đăng nhập", MessageBoxButtons.OK);
                txtPass.Focus();
                return;
            }

            if(txtLogin.Text=="sa" || txtLogin.Text=="HTKN")
            {
                MessageBox.Show("Tài Khoản Không Đúng", "Báo lỗi đăng nhập", MessageBoxButtons.OK);
                return;
            }

            Program.mlogin = txtLogin.Text;
            Program.password = txtPass.Text;
            if (Program.KetNoi() == 0)
            {
                return;
            }
            Program.mChinhanh = cmbCN.SelectedIndex;
            
            Program.mloginDN = Program.mlogin;
            Program.passwordDN = Program.password;
            String strLenh = "exec SP_DANGNHAP '" + Program.mlogin + "'";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if(Program.myReader == null)
            {
                return;
            }

            if(Program.myReader.Read())
            {
                Program.username = Program.myReader.GetString(0);
            }
          
            if(Convert.IsDBNull(Program.username) == true)
            {
                MessageBox.Show("Login bạn không có quyền truy cập dữ liệu\n Bạn xem lại username và password\n");
                Program.myReader.Close();
                return;
            }

            Program.mHoten = Program.myReader.GetString(1);
            Program.mGroup = Program.myReader.GetString(2);
            Program.myReader.Close();
            Program.conn.Close();

            this.Hide();

           
            Program.fmChinh = new fmMain();
            Program.fmChinh.MANV.Text = "Mã nhân viên: " + Program.username;
            Program.fmChinh.HOTEN.Text = "Họ tên: " + Program.mHoten;
            Program.fmChinh.NHOM.Text = "Nhóm: " + Program.mGroup;
          
            Program.fmChinh.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if(txtLogin.Text.Trim() == "" && txtPass.Text.Trim() == "")
            {
                Close();
            }
            else
            {
                DialogResult r = MessageBox.Show("Bạn có chắc chắn muốn thoát", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(r == DialogResult.Yes)
                {
                    Close();
                }
            }
        }

        private void fmDangNhap_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
