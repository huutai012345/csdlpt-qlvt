﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLVT.Reports
{
    public partial class reportHoatDongNhanVien : DevExpress.XtraReports.UI.XtraReport
    {
        public reportHoatDongNhanVien(int maNV, DateTime tuNgay, DateTime denNgay, String loai)
        {
            InitializeComponent();
            txtMaNV.Text = "Mã NV: " + Program.username;
            txtHoTen.Text = "Họ Tên: " + Program.mHoten;
            txtNhom.Text = "Nhóm: " + Program.mGroup;
            this.sqlDataSource1.Queries[0].Parameters[0].Value = maNV;
            this.sqlDataSource1.Queries[0].Parameters[1].Value = tuNgay;
            this.sqlDataSource1.Queries[0].Parameters[2].Value = denNgay;
            this.sqlDataSource1.Queries[0].Parameters[3].Value = loai;
            this.sqlDataSource1.Fill();
        }

    }
}
