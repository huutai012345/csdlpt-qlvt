﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLVT.Class
{
    class KhoModel
    {
        private String maKho;
        private String tenKho;
        private String diaChi;
        private String maCN;

        public KhoModel(String maKho, String tenKho, String diaChi, String maCN)
        {
            this.maKho = maKho;
            this.tenKho = tenKho;
            this.diaChi = diaChi;
            this.maCN = maCN;
        }
    }
}
