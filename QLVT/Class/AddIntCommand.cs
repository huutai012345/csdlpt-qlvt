﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLVT.Class
{
    class AddIntCommand : ICommand<int>
    {
        private int _Value;

        public int Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
            }
        }

        public AddIntCommand()
        {
            _Value = 0;
        }
        public AddIntCommand(int value)
        {
            _Value = value;
        }

        public int Do(int input)
        {
            return input + _Value;
        }
        public int Undo(int input)
        {
            return input - _Value;
        }

    }
}
