﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using QLVT.Validate;

namespace QLVT
{
    public partial class fmVatTu : Form
    {
        private ValidateVatTu validateVatTu;
        int vitri = 0;
        bool isEdit;

        public fmVatTu()
        {
            InitializeComponent();
            gridView1.OptionsBehavior.Editable = false;
            trangThai2();

            if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false;
            }

            this.validateVatTu = new ValidateVatTu();
        }

        private void fmVatTu_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet1.Vattu' table. You can move, or remove it, as needed.
            this.vattuTableAdapter.Fill(this.qLVT_DATHANGDataSet1.Vattu);

            if (bdsVT.Count > 0)
            {
                //this.maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
            }
        }

        private void trangThai1()
        {
            gcVATTU.Enabled = false;
            txtMAVT.Enabled = txtTENVT.Enabled = txtDVT.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                = btnIn.Enabled = btnRefesh.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;
        }

        private void trangThai2()
        {
            txtMAVT.Enabled = txtTENVT.Enabled =  txtDVT.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnIn.Enabled = btnRefesh.Enabled
              = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;
            gcVATTU.Enabled = true;
        }

        private void updateTableAdapter()
        {
            bdsVT.EndEdit();
            bdsVT.ResetCurrentItem();
            this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
            this.vattuTableAdapter.Update(this.qLVT_DATHANGDataSet1.Vattu);
        }

        private void refesh()
        {
            this.vattuTableAdapter.Fill(this.qLVT_DATHANGDataSet1.Vattu);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            vitri = bdsVT.Position;
            bdsVT.AddNew();

            isEdit = false;

        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            vitri = bdsVT.Position;
            isEdit = true;

            this.trangThai1();
            txtMAVT.Enabled = false;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (SP.checkIsDelVT(txtMAVT.Text))
                {
                    MessageBox.Show("Không Thể Xóa", "THÔNG BÁO", MessageBoxButtons.OK);
                }
                else
                {
                    bdsVT.RemoveCurrent();
                    this.updateTableAdapter();
                    MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
                 
                }
            }
   
            trangThai2();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtMAVT.Text = txtMAVT.Text.Trim();
            txtTENVT.Text = txtTENVT.Text.Trim();
            txtDVT.Text = txtDVT.Text.Trim();

            if (!this.validateVatTu.validate(txtMAVT, txtTENVT, txtDVT))
            {
                return;
            }

            if (!isEdit)
            {
                if (SP.checkIsExistMaVT(txtMAVT.Text))
                {
                    MessageBox.Show("Mã Vật tư Đã Tồn Tại", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai1();
                    return;
                }
                if (SP.checkIsExistTenVT(txtTENVT.Text))
                {
                    MessageBox.Show("Tên Vật tư Đã Tồn Tại", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai1();
                    return;
                }
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsVT.RemoveCurrent();
            }
            trangThai2();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["TrangThaiXoa"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["SOLUONGTON"], 0);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
