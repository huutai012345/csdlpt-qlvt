﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QLVT
{
    public partial class fmChuyenCN : DevExpress.XtraEditors.XtraForm
    {
        private bool isRegister;   
        private String name;
        private String loginName;
        private String userName;
        String maCNCurrent;
        String server;

        public fmChuyenCN()
        {
            InitializeComponent();
        }

        public fmChuyenCN(String loginName, String userName,String name, String maCN,bool isRegister)
        {
            InitializeComponent();

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Loi");
            }

            this.loginName = loginName;
            this.userName = userName;
            this.name = name;
            this.isRegister = isRegister;
            maCNCurrent = maCN;

            txtLogin.Text = loginName;
            txtUser.Text = userName;


            if (!isRegister)
            {
                txtPass.Enabled = false;
                txtCPass.Enabled = false;
            }
            else
            {
                txtRole.Text = SP.getRole(userName);
            }

            this.loadData();

        }

        private void loadData()
        {
            cmbMaCN.DataSource = new BindingSource(SP.getChiNhanh(maCNCurrent), null);
            cmbMaCN.DisplayMember = "Value";
            cmbMaCN.ValueMember = "Key";

            cmbMaCN.SelectedIndex = 0;
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDK_Click(object sender, EventArgs e)
        {
            int maNVNew = SP.chuyenCN( int.Parse(this.userName), cmbMaCN.SelectedValue.ToString());

            if(isRegister)
            {
                txtPass.Text = txtPass.Text.Trim();
                txtCPass.Text = txtCPass.Text.Trim();

                if (txtPass.Text == "" || txtCPass.Text == "")
                {
                    MessageBox.Show("Vui Lòng Nhập Mật Khẩu", "THÔNG BÁO", MessageBoxButtons.OK);
                    return;
                }

                if (txtPass.Text != txtCPass.Text)
                {
                    MessageBox.Show("Mật khẩu không khớp", "THÔNG BÁO", MessageBoxButtons.OK);
                    return;
                }

                if (userName == Program.username)
                {
                    MessageBox.Show("Tài khoản này đang được sử dụng!", "THÔNG BÁO", MessageBoxButtons.OK);
                    return;
                }

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;

                if(!SP.delLogin(loginName, userName))
                {
                    return;
                }    

                String servername = Program.servername;
                Program.servername = server;
              
                if (Program.KetNoi() == 0)
                {
                    MessageBox.Show("Lỗi Kết Nối", "THÔNG BÁO", MessageBoxButtons.OK);
                    Program.servername = servername;
                    return;
                }

                if (SP.register(this.name + maNVNew, maNVNew.ToString(), txtCPass.Text, txtRole.Text))
                {
                    MessageBox.Show("Tên Đăng Nhập Mới Của Nhân Viên Ở Chi Nhánh Mới Là :" + this.name + maNVNew, "THÀNH CÔNG", MessageBoxButtons.OK);
                }

                Program.servername = servername;
            }
            else
            {
                MessageBox.Show("Đã Chuyển Thành Công ", "THÔNG BÁo", MessageBoxButtons.OK);
            }
                

            this.Close();
            return;
        }

        private void fmChuyenCN_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_CHINHANH' table. You can move, or remove it, as needed.
            this.v_DS_CHINHANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_CHINHANH);

            cmbServer.SelectedIndex = cmbServer.FindStringExact(cmbMaCN.Text);
            if (cmbServer.SelectedValue == null)
            {
                return;
            }
            server = cmbServer.SelectedValue.ToString();
            //Console.WriteLine(server);
        }

        private void cmbMaCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            String myString = cmbMaCN.Text;
            cmbServer.SelectedIndex = cmbServer.FindStringExact(myString);
            
            if (cmbServer.SelectedValue == null)
            {
                return;
            }
            server = cmbServer.SelectedValue.ToString();
            Console.WriteLine(server);
        }
    }
}