﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace QLVT
{
    public partial class fmPhieuNhap : Form
    {
        private bool isEdit=true;
        private int vitri = 0;
        private string maNV;
        private string maPN;

        public fmPhieuNhap()
        {
  
            InitializeComponent();
            this.maNV = Program.username;
            gridView1.OptionsBehavior.Editable = false;

            trangThai2();

            this.panel1.Visible = false;
            if (Program.mGroup == Program.nhomQuyen[1])
            {
                this.panel1.Visible = true;
            }
        }

        private void fmPhieuNhap_Load(object sender, EventArgs e)
        {
            if (SP.checkIsEmptyCTDDH())
            {
                MessageBox.Show("Hiện Tại Vẫn Chưa Có CTDDH Nào", "Thông Báo", MessageBoxButtons.OK);
                //this.Close();
                this.BeginInvoke(new MethodInvoker(Close));
                return;
            }

            this.v_DS_PHANMANHTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
           
            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuNhap);

            this.v_DS_KHOTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);

            this.v_DS_DATHANGTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_DATHANGTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_DATHANG);

            cmbCN.SelectedIndex = Program.mChinhanh;
            cmbKho.SelectedIndex = 0;
            cmbMSDDH.SelectedIndex = 0;
            btnCTPN.Enabled = true;

            if (bdsPN.Count > 0)
            {
                cmbKho.SelectedValue = ((DataRowView)bdsPN[0])["MAKHO"];
                cmbMSDDH.SelectedValue = ((DataRowView)bdsPN[0])["MasoDDH"];
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
                btnCTPN.Enabled = false;
            }
        }

        private void trangThai1()
        {
            gcPN.Enabled = false;
            txtNgay.Enabled = cmbMSDDH.Enabled = cmbKho.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;

            cmbCN.Enabled = false;
        }

        private void trangThai2()
        {
            gcPN.Enabled = true;
            txtMaPN.Enabled = txtNgay.Enabled = cmbMSDDH.Enabled = cmbKho.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
            = btnRefesh.Enabled = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;

            cmbCN.Enabled = true;
        }

        private void updateTableAdapter()
        {
            bdsPN.EndEdit();
            bdsPN.ResetCurrentItem();
            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Update(this.qLVT_DATHANGDataSet1.PhieuNhap);
        }

        private void refesh()
        {
          
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);

          
           
            this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);

           
            this.v_DS_DATHANGTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_DATHANG);
            this.phieuNhapTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuNhap);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            btnCTPN.Enabled = true;
            vitri = bdsPN.Position;
            bdsPN.AddNew();
            isEdit = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            vitri = bdsPN.Position;
            txtMaPN.Enabled = false;
            cmbMSDDH.Enabled = false;
            isEdit = true;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            DialogResult rs = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
              //  MessageBox.Show(SP.checkIsDelPN(this.maPN).ToString(), "THÔNG BÁO", MessageBoxButtons.OK);
               // Console.WriteLine();
                if(SP.checkIsDelPN(txtMaPN.Text))
                {
                    MessageBox.Show("Không Thể Xóa Vì Đã Có CTPN", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai2();
                    return;
                }

                bdsPN.RemoveCurrent();
                this.updateTableAdapter();
                MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            trangThai2();
            this.refesh();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(!isEdit)
            {   
                if(SP.checkIsExistPN(this.maPN, cmbMSDDH.SelectedValue.ToString()))
                {
                    MessageBox.Show("Phiếu Nhập Đã Tồn Tại", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai1();
                    return;
                }

                SP.updateTTXNV(Program.username);
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsPN.RemoveCurrent();
            }
            trangThai2();

            if (bdsPN.Count <= 0)
            {
                btnCTPN.Enabled = false;
            }
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void btnDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            this.maPN = SP.initMaPN();
            txtMaPN.Text = this.maPN;

            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAPN"], this.maPN);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["NGAY"], txtNgay.Value.Date);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MANV"], this.maNV);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAKHO"], cmbKho.SelectedValue.ToString());
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MasoDDH"], cmbMSDDH.SelectedValue.ToString());
        }

        private void cmbCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCN.SelectedValue != null)
            {
                Program.servername = cmbCN.SelectedValue.ToString();

                if (cmbCN.SelectedIndex != Program.mChinhanh)
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                else
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }

                if (Program.KetNoi() == 0)
                {
                    MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                }
                else
                {
                    this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.phieuNhapTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuNhap);

                    this.v_DS_KHOTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);

                    this.v_DS_DATHANGTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.v_DS_DATHANGTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_DATHANG);
                }
            }
        }

        private void cmbMSDDH_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMSDDH.SelectedValue != null)
            {
                String maDDH = cmbMSDDH.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MasoDDH", maDDH);
                }

            }
        }

        private void cmbKho_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKho.SelectedValue != null)
            {
                String maKho = cmbKho.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAKHO", maKho);
                }

            }
        }

        private void btnCTPN_Click(object sender, EventArgs e)
        {
            if (!isEdit)
            {
                if (SP.checkIsExistPN(this.maPN, cmbMSDDH.SelectedValue.ToString()))
                {
                    MessageBox.Show("Phiếu Nhập Đã Tồn Tại", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai1();
                    return;
                }

                SP.updateTTXNV(Program.username);

                try
                {
                    this.updateTableAdapter();
                    new fmChiTietPhieuNhap(txtMaPN.Text, cmbMSDDH.SelectedValue.ToString()).ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
                }
            }
            else
            {
                new fmChiTietPhieuNhap(txtMaPN.Text, cmbMSDDH.SelectedValue.ToString()).ShowDialog();
            }

            this.trangThai2();
            this.refesh();
        }

        private void gcPN_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                string maKho = gridView1.GetRowCellValue(selectedRowHandles[0], "MAKHO").ToString();
                string maDDH = gridView1.GetRowCellValue(selectedRowHandles[0], "MasoDDH").ToString();

                //Console.WriteLine(maKho);

                cmbKho.SelectedValue = maKho;
                cmbMSDDH.SelectedValue = maDDH;
            }
        }

    }
}