﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using QLVT.Validate;

namespace QLVT
{
    public partial class fmDatHang : Form
    {
        private bool isEdit = true;
        private int vitri = 0;
        private string maDDH;
        private ValidateDH validateDH;

        public fmDatHang()
        {
            InitializeComponent();
            gridView1.OptionsBehavior.Editable = false;
            trangThai2();

            if (Program.mGroup != Program.nhomQuyen[1])
            {
                this.panel1.Visible = false;
            }

            if (Program.mGroup == Program.nhomQuyen[2])
            {
                btnThem.Enabled = btnSua.Enabled = btnXoa.Enabled;
            }

            if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false;
            }
        }

        private void fmDatHang_Load(object sender, EventArgs e)
        {
            if (SP.checkIsEmptyVT())
            {
                MessageBox.Show("Hiện Tại Vẫn Chưa Có VT Nào", "Thông Báo", MessageBoxButtons.OK);
                //this.Close();
                this.BeginInvoke(new MethodInvoker(Close));
                return;
            }

            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet1.V_DS_KHO' table. You can move, or remove it, as needed.
            this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet1.V_DS_KHO' table. You can move, or remove it, as needed.

            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_PHANMANH' table. You can move, or remove it, as needed.
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);

            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_KHO' table. You can move, or remove it, as needed.
            this.v_DS_KHOTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);

            this.datHangTableAdapter.Fill(this.qLVT_DATHANGDataSet1.DatHang);

            cmbCN.SelectedIndex = Program.mChinhanh;
            cmbKho.SelectedIndex = 0;

            if (bdsDH.Count > 0)
            {
                cmbKho.SelectedValue = ((DataRowView)bdsDH[0])["MAKHO"];
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
                btnCTDH.Enabled = false;
            }

            validateDH = new ValidateDH();
        }

        private void trangThai1()
        {
            gcDH.Enabled = false;
            txtNgay.Enabled = txtNhaCC.Enabled = true;
            cmbKho.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                = btnIn.Enabled = btnRefesh.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;
            cmbCN.Enabled = false;

            // btnCTDH.Enabled = true;
        }

        private void trangThai2()
        {
            gcDH.Enabled = true;
            txtMaDDH.Enabled = txtNgay.Enabled = txtNhaCC.Enabled = false;
            cmbKho.Enabled = false;
            // btnCTDH.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnIn.Enabled = btnRefesh.Enabled
              = btnUndo.Enabled = btnRedo.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = false;

            cmbCN.Enabled = true;


            this.isEdit = true;
        }

        private void refesh()
        {
            this.datHangTableAdapter.Fill(this.qLVT_DATHANGDataSet1.DatHang);
        }

        private void updateTableAdapter()
        {
            bdsDH.EndEdit();
            bdsDH.ResetCurrentItem();
            // this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Update(this.qLVT_DATHANGDataSet1.DatHang);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            btnCTDH.Enabled = true;

            vitri = bdsDH.Position;
            bdsDH.AddNew();
            isEdit = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
    
            vitri = bdsDH.Position;
            isEdit = true;

            this.trangThai1();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();

            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (SP.checkIsDelDH(txtMaDDH.Text))
                {
                    MessageBox.Show("Không Thể Xóa Vì Đã Có CTDH", "THÔNG BÁO", MessageBoxButtons.OK);
                    trangThai2();
                    return;
                }
                else
                {
                    bdsDH.RemoveCurrent();
                    this.updateTableAdapter();
                    MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);

                    trangThai2();
                    this.refesh();

                    if (bdsDH.Count <= 0)
                    {
                        btnCTDH.Enabled = false;
                    }

                }
            }
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtNhaCC.Text = txtNhaCC.Text.Trim();

            if (!this.validateDH.validate(txtNhaCC))
            {
                return;
            }

            if(!isEdit)
            {
                SP.updateTTXNV(Program.username);
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsDH.RemoveCurrent();
            }
            trangThai2();

            if (bdsDH.Count <= 0)
            {
                btnCTDH.Enabled = false;
            }
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            this.maDDH = SP.initMaDDH();
            txtMaDDH.Text = this.maDDH;

            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MasoDDH"], this.maDDH);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAKHO"], cmbKho.SelectedValue.ToString());
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["TrangThaiXoa"], 0);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MANV"], Program.username);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["NGAY"], txtNgay.Value.Date);
        }

        private void btnCTDH_Click(object sender, EventArgs e)
        {
            txtNhaCC.Text = txtNhaCC.Text.Trim();

            if (!this.validateDH.validate(txtNhaCC))
            {
                return;
            }

            if (!isEdit)
            {
                try
                {
                    this.updateTableAdapter();
                    new fmChiTietDatHang(txtMaDDH.Text).ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
                }
            }
            else
            {
                new fmChiTietDatHang(txtMaDDH.Text).ShowDialog();
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void gcDH_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                string maKho = gridView1.GetRowCellValue(selectedRowHandles[0], "MAKHO").ToString();

                //Console.WriteLine(maKho);

                cmbKho.SelectedValue = maKho;
                btnCTDH.Enabled = true;
            }

        }

        private void cmbKho_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKho.SelectedValue != null)
            {
                String maKho = cmbKho.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAKHO", maKho);
                }

            }
        }

        private void cbCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCN.SelectedValue != null)
            {
                Program.servername = cmbCN.SelectedValue.ToString();

                if (cmbCN.SelectedIndex != Program.mChinhanh)
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                else
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }

                if (Program.KetNoi() == 0)
                {
                    MessageBox.Show("Loi");
                }
                else
                {

                    this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.datHangTableAdapter.Fill(this.qLVT_DATHANGDataSet1.DatHang);
                }
            }

        }

    }
}
