﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using QLVT.Validate;

namespace QLVT
{
    public partial class fmChiTietPhieuXuat : Form
    {
        private bool isEdit;
        private int vitri = 0;
        private int soLgTemp;
        private string maVTTemp;
        private string maVT;
        private string maPX;
        private ValidateCTPX validateCTPX;

        public fmChiTietPhieuXuat()
        {
            InitializeComponent();

            gridView1.OptionsBehavior.Editable = false;
            gridView2.OptionsBehavior.Editable = false;
        }

        public fmChiTietPhieuXuat(string maPX)
        {
            InitializeComponent();

            gridView1.OptionsBehavior.Editable = false;
            gridView2.OptionsBehavior.Editable = false;

            this.maPX = maPX;
            this.Text = "Chi tiết phiếu xuất " + this.maPX;
            this.gridView1.ActiveFilterString = "StartsWith([MAPX], \'" + this.maPX + "\')";
            gridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            colMAPX.OptionsFilter.AllowFilter = false;
            this.trangThai2();

            if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false; 
            }

            this.validateCTPX = new ValidateCTPX();
        }

        private void fmChiTietPhieuXuat_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet1.Vattu' table. You can move, or remove it, as needed.
            this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
            this.vattuTableAdapter.Fill(this.qLVT_DATHANGDataSet1.Vattu);
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet.V_DS_VATTU' table. You can move, or remove it, as needed.
            this.v_DS_VATTUTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_VATTU);
           
            this.cTPXTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTPXTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTPX);

            cmbVatTu.SelectedIndex = 0;
            if (bdsCTPX.Count > 0)
            {
                cmbVatTu.SelectedValue = ((DataRowView)bdsCTPX[0])["MAVT"];
                this.maVT = cmbVatTu.SelectedValue.ToString();
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
                this.maVT = cmbVatTu.SelectedValue.ToString();
            }

        }

        private void trangThai1()
        {
            gcCTPX.Enabled = false;
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                 = btnRefesh.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;

        }

        private void trangThai2()
        {
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnRefesh.Enabled
              = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;
            gcCTPX.Enabled = true;

        }

        private void refesh()
        {
            this.cTPXTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTPX);
            this.vattuTableAdapter.Fill(this.qLVT_DATHANGDataSet1.Vattu);
        }

        private void updateTableAdapter()
        {
            bdsCTPX.EndEdit();
            bdsCTPX.ResetCurrentItem();
            this.cTPXTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTPXTableAdapter.Update(this.qLVT_DATHANGDataSet1.CTPX);
        }

        private void edit()
        {

            int soLg = int.Parse(txtSoLg.Text);
            if (soLgTemp > soLg)
            {
                SP.updateSoLgVatTu(maVTTemp, soLgTemp - soLg);

            }
            else if (soLgTemp < soLg)
            {
                SP.updateSoLgVatTu(maVTTemp, -(soLg - soLgTemp));
            }

            return ;

        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            vitri = bdsCTPX.Position;
            bdsCTPX.AddNew();

            isEdit = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            vitri = bdsCTPX.Position;
            isEdit = true;

            maVTTemp = maVT;
            soLgTemp = int.Parse(txtSoLg.Text);

            this.trangThai1();
            cmbVatTu.Enabled = false;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();

            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa?", "THÔNG BÁO XÓA", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SP.updateSoLgVatTu(maVTTemp, soLgTemp);

                bdsCTPX.RemoveCurrent();
                this.updateTableAdapter();
                MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            trangThai2();
            this.refesh();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtSoLg.Text = txtSoLg.Text.Trim();
            txtDonGia.Text = txtDonGia.Text.Trim();

            if(!validateCTPX.validate(txtSoLg,txtDonGia))
            {
                return;
            }

            if (!isEdit)
            {
                int soLg = int.Parse(txtSoLg.Text);

                if (soLg > SP.getSoLgVTCuaKho(this.maVT))
                {
                    MessageBox.Show("Số Lượng Vật Tư Không Đủ", "THÔNG BÁO", MessageBoxButtons.OK);

                    this.trangThai2();
                    this.refesh();
                    return;
                }

                if (SP.checkIsExistCTPX(this.maPX, maVT))
                {
                    bdsCTPX.RemoveCurrent();
                    MessageBox.Show("Vật Tư Này Đã Tồn Tại", "THÔNG BÁO", MessageBoxButtons.OK);

                    this.trangThai2();
                    this.refesh();
                    return;
                }

                if(!SP.updateSoLgVatTu(this.maVT, - int.Parse(txtSoLg.Text)))
                {
                    this.trangThai1();
                }
                return;
            }
            else
            {
                this.edit();
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsCTPX.RemoveCurrent();
            }
            trangThai2();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void btnDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAPX"], this.maPX);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAVT"], cmbVatTu.SelectedValue);
        }

        private void gcCTPX_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                string maKho = gridView1.GetRowCellValue(selectedRowHandles[0], "MAVT").ToString();

                cmbVatTu.SelectedValue = maKho;

            }
        }

        private void cmbVatTu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVatTu.SelectedValue != null)
            {
                this.maVT = cmbVatTu.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAVT", this.maVT);
                }

                Console.WriteLine(this.maVT);
            }
        }

    }
}