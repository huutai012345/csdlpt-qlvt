﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using QLVT.Validate;

namespace QLVT
{
    public partial class fmPhieuXuat : Form
    {
        private bool isEdit =true;
        private int vitri = 0;
        private string maNV;
        private string maPX ;
        private ValidatePX validate;

        public fmPhieuXuat()
        {
            InitializeComponent();
            this.maNV = Program.username;
            gridView1.OptionsBehavior.Editable = false;

            trangThai2();

            this.panel1.Visible = false;
            if (Program.mGroup == Program.nhomQuyen[1])
            {
                this.panel1.Visible = true;
            }

            this.validate = new ValidatePX();
        }

        private void fmPhieuXuat_Load(object sender, EventArgs e)
        {
            if (SP.checkIsEmptyVT())
            {
                MessageBox.Show("Hiện Tại Vẫn Chưa Có VT Nào", "Thông Báo", MessageBoxButtons.OK);
                this.BeginInvoke(new MethodInvoker(Close));
                return;
            }

            this.v_DS_PHANMANHTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_PHANMANHTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_PHANMANH);
          
            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuXuat);

            this.v_DS_KHOTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);

            cmbCN.SelectedIndex = Program.mChinhanh;
            cmbKho.SelectedIndex = 0;
            btnCTPX.Enabled = true;

            if (bdsPX.Count > 0)
            {
                cmbKho.SelectedValue = ((DataRowView)bdsPX[0])["MAKHO"];
                this.maPX = ((DataRowView)bdsPX[0])["MAPX"].ToString();
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
                btnCTPX.Enabled = false;
            }
        }

        private void trangThai1()
        {
            gcPX.Enabled = false;

            txtHoTenKH.Enabled = txtNgay.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                = btnRefesh.Enabled = btnUndo.Enabled = btnRedo.Enabled = false;

            cmbKho.Enabled = true;

            cmbCN.Enabled = false;
        }

        private void trangThai2()
        {
            gcPX.Enabled = true;
            txtMaPX.Enabled = txtNgay.Enabled = txtHoTenKH.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
            = btnRefesh.Enabled = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;

            cmbKho.Enabled = false;

            cmbCN.Enabled = true;
        }

        private void updateTableAdapter()
        {
            bdsPX.EndEdit();
            bdsPX.ResetCurrentItem();
            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Update(this.qLVT_DATHANGDataSet1.PhieuXuat);
        }

        private void refesh()
        {
            this.phieuXuatTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuXuat);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            btnCTPX.Enabled = true;
            isEdit = false;
            vitri = bdsPX.Position;
            bdsPX.AddNew();
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            vitri = bdsPX.Position;
            txtMaPX.Enabled = false;
            isEdit = true;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            DialogResult rs = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                if (SP.checkIsDelPX(txtMaPX.Text))
                {
                    MessageBox.Show("Không Thể Xóa Vì Đã Có CTPX", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai2();
                    return;
                }

                bdsPX.RemoveCurrent();
                this.updateTableAdapter();
                MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            trangThai2();
            this.refesh();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtHoTenKH.Text = txtHoTenKH.Text.Trim();

            if (!this.validate.validate(txtHoTenKH))
            {
                return;
            }

            if(!isEdit)
            {
                SP.updateTTXNV(Program.username);
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsPX.RemoveCurrent();
            }
            trangThai2();

            if (bdsPX.Count <= 0)
            {
                btnCTPX.Enabled = false;
            }
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void btnDong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            this.maPX = SP.initMaPX();
            txtMaPX.Text = this.maPX;

            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAPX"], this.maPX);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["NGAY"], txtNgay.Value.Date);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MANV"], this.maNV);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAKHO"], cmbKho.SelectedValue.ToString());
        }
   
        private void btnCTPX_Click(object sender, EventArgs e)
        {
            txtHoTenKH.Text = txtHoTenKH.Text.Trim();

            if (!this.validate.validate(txtHoTenKH))
            {
                return;
            }

            if (!isEdit)
            {
                try
                {
                    this.updateTableAdapter();
                    new fmChiTietPhieuXuat(this.maPX).ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
                }
            }
            else
            {
                new fmChiTietPhieuXuat(this.maPX).ShowDialog();
            }

            this.trangThai2();
            this.refesh();
          
        }

        private void gcPX_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                string maKho = gridView1.GetRowCellValue(selectedRowHandles[0], "MAKHO").ToString();
                this.maPX= gridView1.GetRowCellValue(selectedRowHandles[0], "MAPX").ToString();

                cmbKho.SelectedValue = maKho;

                btnCTPX.Enabled = true;
            }
        }

        private void cmbCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCN.SelectedValue != null)
            {
                Program.servername = cmbCN.SelectedValue.ToString();

                if (cmbCN.SelectedIndex != Program.mChinhanh)
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                else
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }

                if (Program.KetNoi() == 0)
                {
                    MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
                }
                else
                {
                    this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.phieuXuatTableAdapter.Fill(this.qLVT_DATHANGDataSet1.PhieuXuat);


                    this.v_DS_KHOTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.v_DS_KHOTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_KHO);
                }
            }
        }

        private void cmbKho_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKho.SelectedValue != null)
            {
                String maKho = cmbKho.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAKHO", maKho);
                }

            }
        }

    }
}