﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;

namespace QLVT
{
    public partial class fmChiTietDatHang : Form
    {
        private ValidateCTDH validateCTDH;
        private string maDDH;
        private string maVT;
        private bool isEdit;
        private int vitri = 0;

        public fmChiTietDatHang()
        {
            InitializeComponent();
            gridView1.OptionsBehavior.Editable = false;
            validateCTDH = new ValidateCTDH();

            this.trangThai2();
        }

        public fmChiTietDatHang(String maDDH)
        {
            InitializeComponent();
            this.maDDH = maDDH;
            this.Text = "Chi tiết đơn đặt hàng " + this.maDDH;
            gridView1.OptionsBehavior.Editable = false;

            this.gridView1.ActiveFilterString = "StartsWith([MasoDDH], \'" + this.maDDH + "\')";
            gridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            colMasoDDH.OptionsFilter.AllowFilter = false;
            this.trangThai2();

            txtSum.Text = SP.getSumPriceDDH(maDDH).ToString();

            if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false;
            }

            validateCTDH = new ValidateCTDH();
        }

        private void fmChiTietDatHang_Load(object sender, EventArgs e)
        {
            this.cTDDHTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTDDHTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTDDH);
            this.v_DS_VATTUTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_VATTUTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_VATTU);
           

            cmbVatTu.SelectedIndex = 0;

            if (bdsCTDDH.Count > 0)
            {
                cmbVatTu.SelectedValue = ((DataRowView)bdsCTDDH[0])["MAVT"];
            }
            else
            {
                btnSua.Enabled = btnXoa.Enabled = false;
            }

        }

        private void trangThai1()
        {
            gcCTDDH.Enabled = false;
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled  = false;
      
        }

        private void trangThai2()
        {
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnRefesh.Enabled
              = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;
            gcCTDDH.Enabled = true;
            
        }

        private void refesh()
        {
            this.cTDDHTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTDDH);
            this.v_DS_VATTUTableAdapter.Fill(this.qLVT_DATHANGDataSet.V_DS_VATTU);
            txtSum.Text = SP.getSumPriceDDH(maDDH).ToString();
        }

        private void updateTableAdapter()
        {
            bdsCTDDH.EndEdit();
            bdsCTDDH.ResetCurrentItem();
            this.cTDDHTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTDDHTableAdapter.Update(this.qLVT_DATHANGDataSet1.CTDDH);
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            vitri = bdsCTDDH.Position;
            bdsCTDDH.AddNew();

            isEdit = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(SP.checkIsDelCTDDH(this.maDDH))
            {
                MessageBox.Show("Không Thể Sửa Vì Đã Có Phiếu Nhập", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            vitri = bdsCTDDH.Position;
            isEdit = true;

            this.trangThai1();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();
            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa", "THÔNG BÁO", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if(SP.checkIsDelCTDDH(this.maDDH))
                {
                    MessageBox.Show("Không Thể Xóa Vì Đã Có Phiếu Nhập", "THÔNG BÁO", MessageBoxButtons.OK);
                    this.trangThai2();
                    return;
                }

                bdsCTDDH.RemoveCurrent();
                this.updateTableAdapter();
                MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            trangThai2();
            this.refesh();
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtSoLg.Text = txtSoLg.Text.Trim();
            txtDonGia.Text = txtDonGia.Text.Trim();

            if(!validateCTDH.validate(txtSoLg,txtDonGia))
            {
                return;
            }

            if (!isEdit)
            {
                if(SP.checkIsExistCTDDH(this.maDDH, maVT))
                {
                    bdsCTDDH.RemoveCurrent();
                    MessageBox.Show("Vật tư này đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);
                   
                    this.refesh();
                    this.trangThai2();
                    return;
                }
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO\n", MessageBoxButtons.OK);
            }

            this.refesh();
            this.trangThai2();
           
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsCTDDH.RemoveCurrent();
            }
            trangThai2();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();  
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            maVT = cmbVatTu.SelectedValue.ToString();
            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAVT"], maVT);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MasoDDH"], this.maDDH);
        }

        private void gcCTDDH_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if(selectedRowHandles.Length>0)
            {
                maVT = gridView1.GetRowCellValue(selectedRowHandles[0], "MAVT").ToString();

                cmbVatTu.SelectedValue = maVT;
            }
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void cmbVatTu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVatTu.SelectedValue != null)
            {
                maVT = cmbVatTu.SelectedValue.ToString();
                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAVT", maVT);
                }
            }
        }
    
    }
}
