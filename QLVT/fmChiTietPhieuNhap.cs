﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using QLVT.Validate;

namespace QLVT
{
    public partial class fmChiTietPhieuNhap : Form
    {
        private String maVT;
        private bool isEdit;
        private int vitri = 0;
        private int soLgTemp;
        private String maVTTemp;
        private String maPN;
        private String maDDH;
        private ValidateCTPN validateCTPN;

        public fmChiTietPhieuNhap(String maPN, String maDDH)
        {
            InitializeComponent();

            this.maPN = maPN;
            this.maDDH = maDDH;
            this.Text = "Chi tiết phiếu nhập " + this.maPN;

            gridView1.OptionsBehavior.Editable = false;
            gridView2.OptionsBehavior.Editable = false;

            this.gridView1.ActiveFilterString = "StartsWith([MAPN], \'" + this.maPN + "\')";
            this.gridView2.ActiveFilterString = "StartsWith([MasoDDH], \'" + this.maDDH + "\')";

            gridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            gridView2.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;

            colMAPN.OptionsFilter.AllowFilter = false;
            colMasoDDH.OptionsFilter.AllowFilter = false;

            this.trangThai2();

            if (bdsCTPN.Count > 0)
            {
                cmbVatTu.SelectedValue = ((DataRowView)bdsCTPN[0])["MAVT"];
            }

            if (Program.mGroup == Program.nhomQuyen[1])
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = false;
            }


            validateCTPN = new ValidateCTPN();

        }

        private void fmChiTietPhieuNhap_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLVT_DATHANGDataSet1.V_DS_VTDDH' table. You can move, or remove it, as needed.
            this.v_DS_VTDDHTableAdapter.Connection.ConnectionString = Program.connstr;
            this.v_DS_VTDDHTableAdapter.Fill(this.qLVT_DATHANGDataSet1.V_DS_VTDDH);

            this.cTPNTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTPNTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTPN);


            DataTable ds = Program.ExecSqlDataTable("SELECT * FROM FN_VatTuTheoDDH('" + this.maDDH + "')");
            cmbVatTu.DataSource = ds;
            cmbVatTu.DisplayMember = "TENVT";
            cmbVatTu.ValueMember = "MAVT";
            cmbVatTu.SelectedIndex = 0;

            this.maVT = cmbVatTu.SelectedValue.ToString();
        }

        private void trangThai1()
        {
            gcCTPN.Enabled = false;
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = true;

            btnGhi.Enabled = btnHuy.Enabled = true;
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
                 = btnRefesh.Enabled
                = btnUndo.Enabled = btnRedo.Enabled = false;

        }

        private void trangThai2()
        {
            cmbVatTu.Enabled = txtSoLg.Enabled = txtDonGia.Enabled = false;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled
              = btnRefesh.Enabled
              = btnUndo.Enabled = btnRedo.Enabled = true;
            btnGhi.Enabled = btnHuy.Enabled = false;
            gcCTPN.Enabled = true;

        }

        private void refesh()
        {
            this.cTPNTableAdapter.Fill(this.qLVT_DATHANGDataSet1.CTPN);
        }

        private void updateTableAdapter()
        {
            bdsCTPN.EndEdit();
            bdsCTPN.ResetCurrentItem();
            this.cTPNTableAdapter.Connection.ConnectionString = Program.connstr;
            this.cTPNTableAdapter.Update(this.qLVT_DATHANGDataSet1.CTPN);
        }

        private bool checkIsEdit()
        {
            int soLg = int.Parse(txtSoLg.Text);
            if (soLgTemp > soLg)
            {
                if (SP.getSoLgVTCuaKho(maVTTemp) >= soLgTemp - soLg)
                {
                    SP.updateSoLgVatTu(maVTTemp, -(soLgTemp - soLg));
                    return true;
                }
                else
                {
                    MessageBox.Show("Số Lượng Vật Tư Còn Lại Trong Kho Không Đủ", "THÔNG BÁO", MessageBoxButtons.OK);
                }
            }
            else if (soLgTemp < soLg)
            {

                if (SP.getSoLgVTCuaDH(maVTTemp,this.maDDH) >= soLg - soLgTemp)
                {
                    SP.updateSoLgVatTu(maVTTemp, soLg - soLgTemp);
                    return true;
                }
                else
                {
                    MessageBox.Show("Số Lượng Vật Tư Còn Lại Trong Đơn Đặt Hàng Không Đủ", "THÔNG BÁO", MessageBoxButtons.OK);
                }
            }
            else
            {
                return true;
            }

            return false;

        }

        private bool checkIsDelete()
        {

            if (SP.getSoLgVTCuaKho(maVTTemp) >= soLgTemp)
            {
                SP.updateSoLgVatTu(maVTTemp, soLgTemp);
                return true;
            }

            MessageBox.Show("Số Lượng Vật Tư Còn Lại Trong Kho Khônd Đủ", "THÔNG BÁO", MessageBoxButtons.OK);
            return false;

        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.trangThai1();
            vitri = bdsCTPN.Position;
            bdsCTPN.AddNew();

            isEdit = false;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            vitri = bdsCTPN.Position;
            isEdit = true;

            maVTTemp = maVT;
            soLgTemp = int.Parse(txtSoLg.Text);

            this.trangThai1();
            cmbVatTu.Enabled = false;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            trangThai1();

            DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa?", "THÔNG BÁO XÓA", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                maVTTemp = maVT;
                soLgTemp = int.Parse(txtSoLg.Text);

                if (!this.checkIsDelete())
                {
                    this.trangThai2();
                    return;
                }

                bdsCTPN.RemoveCurrent();
                this.updateTableAdapter();
                MessageBox.Show("Xóa Thành Công", "THÔNG BÁO", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void btnRefesh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.refesh();
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtSoLg.Text = txtSoLg.Text.Trim();
            txtDonGia.Text = txtDonGia.Text.Trim();

            if (!this.validateCTPN.validate(txtSoLg, txtDonGia))
            {
                // MessageBox.Show("Số Lượng Vật Tư Trong DDH Không Đủ", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }


            int soLgVTCL = SP.getSoLgVTCuaDH(maVT,this.maDDH);

            if (soLgVTCL < float.Parse(txtSoLg.Text))
            {
                MessageBox.Show("Số Lượng Vật Tư Trong DDH Không Đủ", "THÔNG BÁO", MessageBoxButtons.OK);

                this.trangThai2();
                return;
            }

            if (!isEdit)
            {
                if (SP.checkIsExistCTPN(this.maPN, maVT))
                {
                    bdsCTPN.RemoveCurrent();
                    MessageBox.Show("Vật tư này đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);

                    this.trangThai2();
                    this.refesh();
                    return;
                }

                SP.updateSoLgVatTu(maVT, int.Parse(txtSoLg.Text));
            }
            else
            {
                if (!checkIsEdit())
                {
                    this.trangThai1();
                    return;
                }
            }

            try
            {
                this.updateTableAdapter();
                MessageBox.Show("Ghi thành công", "THÔNG BÁO GHI", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ghi thất bại\n" + ex.Message + "", "THÔNG BÁO GHI\n", MessageBoxButtons.OK);
            }

            this.trangThai2();
            this.refesh();
        }

        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1 = sender as GridView;
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAPN"], this.maPN);
            gridView1.SetRowCellValue(e.RowHandle, gridView1.Columns["MAVT"], cmbVatTu.SelectedValue);
        }

        private void btnHuy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!isEdit)
            {
                bdsCTPN.RemoveCurrent();
            }
            trangThai2();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void gcCTPN_Click(object sender, EventArgs e)
        {
            int[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                string maKho = gridView1.GetRowCellValue(selectedRowHandles[0], "MAVT").ToString();

                cmbVatTu.SelectedValue = maKho;
            }
        }

        private void cmbVatTu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVatTu.SelectedValue != null)
            {
                this.maVT = cmbVatTu.SelectedValue.ToString();

                int[] selectedRowHandles = gridView1.GetSelectedRows();
                if (selectedRowHandles.Length > 0)
                {
                    gridView1.SetRowCellValue(selectedRowHandles[0], "MAVT", this.maVT);
                }
            }
        }

    }
}