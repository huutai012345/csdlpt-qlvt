﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLVT
{
    static class SP
    {
        public static bool checkIsExistCTDDH(String maDTH, String maVT)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[sp_KiemtraCTDH_TonTai] \n" +
                            "@MasoDDH = N'" + maDTH + "' , @MaVT = N'" + maVT + "' \n" +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistPN(String maPN, String maDDH)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_KiemtraPNTonTai] \n" +
                            "@MaDDH  = N'" + maDDH + "' \n" +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistCTPN(String maPX, String maVT)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_KiemtraCTPNTonTai] \n" +
                            "@MAPN = N'" + maPX + "' , @MaVT = N'" + maVT + "' \n" +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistCTPX(String maPX, String maVT)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_KiemtraCTPXTonTai] \n" +
                            "@MAPX = N'" + maPX + "' , @MaVT = N'" + maVT + "' \n" +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistAccount(String userName)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC	@return_value = [dbo].[SP_KiemtraTaiKhoan_TonTai] \n" +
                                "@UserName =N'" + userName + "'\n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistNhanVien(String maNV)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC	@return_value = [dbo].[SP_KiemtraMaNV_TonTai] \n" +
                                "@MANV =" + Int16.Parse(maNV) + "\n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistKho(String tenKho, String diaChi)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                 "EXEC	@return_value = [dbo].[SP_KiemtraKho_TonTai] \n" +
                                 "@TENKHO =N'" + tenKho + "', " +
                                 "@DIACHI =N'" + diaChi + "' " +
                                 "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistMaVT(String maVT)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[sp_KiemtraMaVT_TonTai] \n" +
                                "@MAVT =N'" + maVT + "'\n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsExistTenVT(String tenVT)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[sp_KiemtraVatTu_TonTai] \n" +
                                "@TENVT =N'" + tenVT + "'\n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsEmptyCTDDH()
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[sp_KiemtraListCTDH] \n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsEmptyVT()
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[sp_KiemtraListVT] \n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelKho(String maKho)
        {
            String strLenh = "DECLARE	@return_value int \n"
                                + "EXEC @return_value = [dbo].[SP_KiemtraXoaKho] \n"
                                + "@MAKHO =N'" + maKho + "' \n"
                                + "SELECT 'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelVT(String maVT)
        {
            Console.WriteLine(maVT + "asdasd");
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[SP_KiemtraXoaVT] \n" +
                                "@MAVT =N'" + maVT + "'\n" +
                                "SELECT  'Return Value' = @return_value \n";


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelDH(String maDDH)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                 "EXEC @return_value = [dbo].[SP_KiemtraXoaDH] \n" +
                                 "@MADDH =N'" + maDDH + "'\n" +
                                 "SELECT  'Return Value' = @return_value \n";


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelCTDDH(String maCTDDH)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[SP_KiemtraXoaCTDDH]" +
                                "@MADDH =N'" + maCTDDH + "'\n" +
                                "SELECT  'Return Value' = @return_value \n";


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelPN(String maPN)
        {
            String strLenh = "DECLARE @return_value int \n" +
                               "EXEC @return_value  = [dbo].[SP_KiemtraXoaPN] \n" +
                               "@MAPN =N'" + maPN + "'\n" +
                               "SELECT  'Return Value' = @return_value \n";


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                Console.WriteLine(maPN);
                Console.WriteLine(Program.myReader.GetInt32(0));
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool checkIsDelPX(String maPX)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC @return_value = [dbo].[SP_KiemtraXoaPX] \n" +
                                "@MAPX =N'" + maPX + "'\n" +
                                 "SELECT  'Return Value' = @return_value \n";


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) == 1)
                {
                    Program.myReader.Close();
                    return true;
                }

            }

            Program.myReader.Close();
            return false;
        }

        public static bool updateSoLgVatTu(String maVT, int soLg)
        {
            String strLenh = "EXEC [dbo].[SP_UpdateSoLgVT] \n" +
                                "@MAVT = N'" + maVT + "', " +
                                "@SLTON = " + soLg;


            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) > 0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool updateCTDDH(String maDTH, String maVT, int soLg)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_UpdateChiTietDH] \n" +
                            "@MasoDDH = N'" + maDTH + "', " +
                            "@MaVT = N'" + maVT + "', " +
                            "@SoLg = " + soLg +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) > 0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool updateCTDPN(String maPN, String maVT, int soLg, float donGia)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_UpdateChiTietPN] \n" +
                            "@MaPN = N'" + maPN + "', " +
                            "@MaVT = N'" + maVT + "', " +
                            "@SoLg = " + soLg + "', " +
                            "@DonGia = " + donGia + "\n " +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) > 0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool updateCTDPX(String maPX, String maVT, int soLg, float donGia)
        {
            String strLenh = "DECLARE @return_value int \n" +
                            "EXEC @return_value = [dbo].[SP_UpdateChiTietPX] \n" +
                            "@MaPX = N'" + maPX + "', " +
                            "@MaVT = N'" + maVT + "', " +
                            "@SoLg = " + soLg + ", " +
                            "@DonGia = " + donGia + "\n " +
                            "SELECT  'Return Value' = @return_value";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) > 0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool updateTTXNV(String maNV)
        {
            String strLenh = "EXEC [dbo].[SP_UpdateTTXNV] \n" +
                                "@MANV = N'" + maNV + "'";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }

            if (Program.myReader.Read())
            {
                if (Program.myReader.GetInt32(0) > 0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static int initMaNV()
        {
            int maNV = 1;
            string strLenh = "EXEC SP_SINHMA_NV";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return maNV;
            }

            if (Program.myReader.Read())
            {
                maNV = Program.myReader.GetInt32(0);
            }

            Program.myReader.Close();
            return maNV;
        }

        public static string initMaDDH()
        {
            String maDH = "";
            string strLenh = "EXEC SP_SINHMA_DH";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader.Read())
            {
                maDH = Program.myReader.GetString(0);
            }
            Program.myReader.Close();

            string str = "MDDH";
            int num = int.Parse(maDH.Substring(4, 4)) + 1;
            int lengOfNum = (num + "").Length;
            for (int i = 0; i < 4 - lengOfNum; i++)
            {
                str = str + "0";
            }
            str = str + num;
            return str;
        }

        public static string initMaPN()
        {
            String maPN = "";
            string str = "PN";
            string strLenh = "EXEC SP_SINHMA_PN";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    maPN = Program.myReader.GetString(0);
                    Program.myReader.Close();
                }

                int num = int.Parse(maPN.Substring(2, 6)) + 1;
                int lengOfNum = num.ToString().Length;
                for (int i = 0; i < 6 - lengOfNum; i++)
                {
                    str = str + "0";
                }
                str = str + num;
            }

            return str;
        }

        public static string initMaPX()
        {
            String maPN = "";
            string str = "PX";
            string strLenh = "EXEC SP_SINHMA_PX";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    maPN = Program.myReader.GetString(0);
                    Program.myReader.Close();
                }

                int num = int.Parse(maPN.Substring(2, 6)) + 1;
                int lengOfNum = num.ToString().Length;
                for (int i = 0; i < 6 - lengOfNum; i++)
                {
                    str = str + "0";
                }
                str = str + num;
            }

            return str;
        }

        public static String initMaKho()
        {
            String maKho = "";
            string strLenh = "EXEC SP_SINHMA_KHO ";
            Program.myReader = Program.ExecSqlDataReader(strLenh);

            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    maKho = Program.myReader.GetString(0);
                }


                Program.myReader.Close();
            }


            String num = "";
            maKho = maKho.Substring(2, 2);
            int n = int.Parse(maKho) + 1;
            if (n < 10)
            {
                num = "0" + n;
            }
            if (Program.mChinhanh == 0)
            {
                maKho = "N" + 1 + num;
            }
            else
            {
                maKho = "N" + 2 + num;
            }

            return maKho;
        }

        public static bool register(String login, String user, String pass, String role)
        {
            String strLenh = "DECLARE @return_value int \n" +
                                "EXEC	@return_value = [dbo].[Sp_TaoTaiKhoan] \n" +
                                "@LGNAME = N'" + login + "', " +
                                "@PASS = N'" + pass + "', " +
                                "@USERNAME = N'" + user + "', " +
                                "@ROLE = N'" + role + "' \n" +
                                "SELECT  'Return Value' = @return_value \n";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return false;
            }
            if (Program.myReader.Read())
            {
                if(Program.myReader.GetInt32(0)==0)
                {
                    Program.myReader.Close();
                    return true;
                }
            }

            Program.myReader.Close();
            return false;
        }

        public static bool delLogin(String loginName, String userName)
        {
            String strLenh = "EXEC [dbo].[SP_XOATAIKHOAN] \n" +
                                 "@LGNAME   ='" + loginName + "',\n" +
                                 "@USRNAME  ='" + userName + "'"; 

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                Program.myReader.Close();
                return true;
            }
            return false;
        }

        public static int chuyenCN(int maNV, String maCN)
        {
            int maNVNew = maNV;
            String strLenh = "EXEC [dbo].[SP_CHUYENCN] \n" +
                                "@MANV =" + maNV + ",\n" +
                                "@MACN =N'" + maCN + "'";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    maNVNew = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
            }

           
            return maNVNew;
        }

        public static List<KeyValuePair<string, string>> getChiNhanh(String maCN)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            string strLenh = "EXEC	[dbo].[sp_LAYCHINHANH]\n" +
                                    "@MaChiNhanh = N'" + maCN + "'";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null)
            {
                return list;
            }

            while (Program.myReader.Read())
            {
                list.Add(new KeyValuePair<string, string>(Program.myReader.GetString(0), Program.myReader.GetString(1)));
            }

            Program.myReader.Close();

            return list;

        }

        public static string getRole(String userName)
        {
            string role = "";
            string strLenh = "EXEC SP_LAYROLE \n" +
                            "@USERNAME = N'" + userName + "'";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    role = Program.myReader.GetString(0);
                }
            }

            Program.myReader.Close();
            return role;
        }

        public static int getSoLgVTCuaDH(String maVT,String maSoDDH)
        {
            int soLg = 0;

            string strLenh = "EXEC SP_LaySoLgVTDH \n" +
                                "@MAVT = N'" + maVT + "', "+
                                "@MasoDDH =N'"+maSoDDH+"'";

            Program.myReader = Program.ExecSqlDataReader(strLenh);

            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    soLg = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
            }

            return soLg;
        }

        public static int getSoLgVTCuaKho(String maVT)
        {
            int soLg = 0;

            string strLenh = "EXEC SP_LaySoLgVTKho \n" +
                                "@MAVT = N'" + maVT + "'";

            Program.myReader = Program.ExecSqlDataReader(strLenh);

            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    soLg = Program.myReader.GetInt32(0);
                }
                Program.myReader.Close();
            }

            return soLg;
        }

        public static float getSumPriceDDH(String maDDH)
        {
            float sum = 0;

            string strLenh = "EXEC SP_TongGiaTriDDH \n" +
                            "@MADDH = N'" + maDDH + "'";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader != null)
            {
                if (Program.myReader.Read())
                {
                    sum = (float)Program.myReader.GetDouble(0);

                }
            }

            Program.myReader.Close();
            return sum;
        }

    }
}
